var application_list = [
  {"name": "Photoshop", "desc": "Application", "icon": "media/img/ps.png"},
  {"name": "Firefox", "desc": "Application", "icon": "media/img/firefox.png"},
  {"name": "Illustrator", "desc": "Application", "icon": "media/img/ai.png"},
  {"name": "Chrome", "desc": "Application", "icon": "media/img/chrome.png"},
  {"name": "Sublime text 2", "desc": "Application", "icon": "media/img/sl2.png"},
  {"name": "Eclipse", "desc": "Application", "icon": "media/img/eclipse.png"},
  {"name": "Spotify", "desc": "Application", "icon": "media/img/spotify.png"},
  {"name": "App Store", "desc": "Application", "icon": "media/img/astore.png"}
]






var PINCH_THRESHOLD = 10;

var _state = 0;

var _BASE = 0;
var _APPLOADER = 1;
var _SEARCH = 2;
// var _OPENAPPS = 3;

var _TUTORIAL = true

// Private vars
// var _pinch = {is: false, rad: 0, prev_rad: 100, ctr: 0};
var _swipe = {dir: ""};

var _kill = null;
var _killTimer = null;


Leap.loop({enableGestures: true}, function(frame) {
  if (frame.gestures.length > 0) {
    var gesture = frame.gestures[0];

    if (gesture.type == "swipe") {
      var isHorizontal = Math.abs(gesture.direction[0]) > Math.abs(gesture.direction[1]);
      
      if (isHorizontal) {
        if (gesture.direction[0] > 0) {
          if (_kill == "SWIPE_RIGHT" && should_kill()) {
            console.log("KILLED RIGHT")
            return;
          }

          _swipe.dir = "right";
        } else {
          if (_kill == "SWIPE_LEFT" && should_kill()) {
            console.log("KILLED LEFT")
            return;
          }

          _swipe.dir = "left";
        }
      } else if(gesture.direction[1] > 0) {
        if (_kill == "SWIPE_UP" && should_kill()) {
          console.log("KILLED UP")
          return;
        }

        _swipe.dir = "up";
      } else {
        if (_kill == "SWIPE_DOWN" && should_kill()) {
          console.log("KILLED DOWN")
          return;
        }

        _swipe.dir = "down";
      }

      do_swipe();
    }
  } else if (frame.hands.length > 0) {
    var _hand = frame.hands[0];

    console.log(_hand.pinchStrength)
    if (_hand.pinchStrength > 0.9) {
      do_pinch();
    }


    // _pinch.rad = frame.hands[0].sphereRadius;

    // if (_pinch.is) {
    //   if (_pinch.rad < _pinch.prev_rad) {
    //     _pinch.ctr++;

    //     if (_pinch.ctr >= PINCH_THRESHOLD) {
    //       _pinch.is = false;
    //       do_pinch();
    //     }

    //     _pinch.prev_rad = _pinch.rad;
    //   } else {
    //     _pinch.ctr = 0;
    //     _pinch.is = false;
    //     _pinch.prev_rad = _pinch.rad;
    //     return;
    //   }
    // } else {
    //   _pinch.prev_rad = _pinch.rad;
    //   _pinch.is = true;
    // }
  } else {
    _kill = null;
    _killTimer = null;
  }
})

do_swipe = function() {
  console.log(_swipe);
  // Add killer
  switch (_swipe.dir) {
    case "up":
      _kill = "SWIPE_UP";
      break;

    case "down":
      _kill = "SWIPE_DOWN";
      break;

    case "left":
      _kill = "SWIPE_LEFT";
      break;

    case "right":
      _kill = "SWIPE_RIGHT";
      break;
  }

  _killTimer = now();

  switch (_state) {
    case _BASE:
      switch (_swipe.dir) {
        case "up":
          // $('.menubar').slideUp();
          $('.apploader').slideDown();
          _state = _APPLOADER;
          tut();
          return;

        case "down":
          $('.menubar').slideUp();
          $('.search').slideDown();
          $('.search-field').focus();
          _state = _SEARCH;
          tut();
          return;
      }
      break;

    case _APPLOADER:
      switch (_swipe.dir) {
        case "down":
          // $('.menubar').slideDown();
          $('.apploader').slideUp();
          _state = _BASE;
          tut();
          return;
        case "left":
          $th = $('.app-selected');

          if ($th.is(':first-child')) {
            $th.removeClass('app-selected');
            $('.app-visited:last-child').addClass('app-selected')
          } else {
            $th.removeClass('app-selected').prev().addClass('app-selected')
          }
          return;

        case "right":
          $th = $('.app-selected');
          
          if ($th.is(':last-child')) {
            $th.removeClass('app-selected');
            $('.app-visited:first-child').addClass('app-selected')
          } else {
            $th.removeClass('app-selected').next().addClass('app-selected')
          }
          return;
      }
      break;

    case _SEARCH:

      switch (_swipe.dir) {
        case "up":
          $('.menubar').slideDown();
          $('.search-loader').empty();
          $('.search').slideUp();
          _state = _BASE;
          tut(false);

          return;

        case "left":
          $th = $('.search-top-hit');

          if ($th.is(':first-child')) {
            $th.removeClass('search-top-hit');
            $('.search-result:last-child').addClass('search-top-hit')
          } else {
            $th.removeClass('search-top-hit').prev().addClass('search-top-hit')
          }
          return;

        case "right":
          $th = $('.search-top-hit');
          
          if ($th.is(':last-child')) {
            $th.removeClass('search-top-hit');
            $('.search-result:first-child').addClass('search-top-hit')
          } else {
            $th.removeClass('search-top-hit').next().addClass('search-top-hit')
          }
          return;
      }
      return;
  }
}

do_pinch = function() {
  if (_kill == "PINCH" && should_kill()) {
    return;
  }
  _kill = "PINCH";
  _killTimer = now() + 800;

  switch (_state) {
    case _APPLOADER:
      $th = $('.app-selected');
      
      if ($th.is(':last-child')) {
        $th.removeClass('app-selected');
        $('.app-visited:first-child').addClass('app-selected')
      } else {
        $th.removeClass('app-selected').next().addClass('app-selected')
      }

      $th.remove()
      break;
  }

}

now = function() {
  return new Date().getTime();
}

should_kill = function() {
  return _killTimer >= now() - 400;
}

$(function() {
  tut(true)

  $(document).on('keyup', function() {
    // Only filter if you're in the app loader state
    if (_state != _APPLOADER)
      return;


  })
  $('.search-field').on('keyup', function() {
    if (_state != _SEARCH)
      return;

    var query = $(this).val();

    $('.search-loader').empty();

    if (! query) {
      return;
    }

    var result_set = new Array();
    for (idx in application_list) {
      var app = application_list[idx];

      if (new RegExp(query, "i").test(app.name)) {
        result_set.push(app);
      }
    }

    for (idx in result_set) {
      var app = result_set[idx];

      $('.search-loader').append($('<div>').addClass("search-result")
                                    .append($('<img>').attr({"class": "result-app-icon", "src": app.icon}))
                                    .append($('<div>').addClass('result-app-name').html(app.name))
                                    .append($('<div>').addClass('result-desc').html(app.desc)))

    }

    $('.search-result:first').addClass('search-top-hit')
  })
})

var growl = function(title, message, dur) {
  if (dur === undefined) {
    dur = 10000
  }
  $.growl({title: title, message: message, close: '', duration: dur, style: "info"})
}

var tut = function(sp) {
  $('#growls').empty()
  if (_TUTORIAL) {
    switch(_state) {
      case _BASE:
        if (sp) {
          growl('Hello', 'guinx is now connected! Swipe down to begin')
        } else {
          growl('Welcome back', 'Now swipe up again')
        }
        break;

      case _SEARCH:
        growl('Search', 'This is the search display', 5000)
        growl('', 'Start typing in the search box to display available applications', 10000)
        growl('', 'Swipe left and right to navigate through results', 15000)
        growl('', 'Swipe up to go back to your desktop', 20000)
        break;

      case _APPLOADER:

        growl('App Loader', 'This is the app loader', 5000)
        growl('', 'Swipe left and right to navigate open apps', 10000)
        growl('', 'Pinch to close app', 15000)
        growl('', 'Swipe down again to go back to your desktop')
        _TUTORIAL = false
        break;
    }
  }
}